﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Module3_2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world!");
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            if(!Int32.TryParse(input,out result) ||  result<0)
            {
                return false;
            }
            return true;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] array = new int[n];
            
            if (n == 0)
                return array;
            array[0] = 0;
            if (n==1)
                return array;
            array[1] = 1;
            for (int i =2;i<n; i++)
            {
                array[i] = array[i - 1] + array[i - 2];
            }
            return array;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            bool minus = false;
            if(sourceNumber<0)
            {
                minus = true;
                sourceNumber *= (-1);
            }
            string s="";
            for(int i=sourceNumber.ToString().Length;i>0;i--)
            {
                s += sourceNumber.ToString()[i - 1];
            }
            if (minus== true)
            {
                return Convert.ToInt32('-' + s);
            }
            else
            {
                return Convert.ToInt32(s);
            }
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = null;
            if (size < 0)
            {
                return array;
            }
            Random random = new Random();
            array = new int[size];
            for (int i =0;i<size;i++)
            {
                array[i] = random.Next(-5, 5);
            }
            return array;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            for(int i=0;i<source.Length; i++)
            {
                if(source[i]!=0)
                {
                    source[i] *= -1;
                }
            }
            return source;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] array = null;
            if (size < 0)
            {
                return array;
            }
            Random random = new Random();
            array = new int[size];
            for (int i = 0; i < size; i++)
            {
                array[i] = random.Next(0, 10);
            }
            return array;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> list = new List<int>();
            for(int i=1;i<source.Length;i++)
            {
                if(source[i]>source[i-1])
                {
                    list.Add(source[i]);
                }
            }
            return list;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] array = new int[size, size];
            int max = size * size;
            int r = 1;
            int rows = 0;
            int columb = 0;
            for(int h=0;h<=size/2;h++)
            {
                int i;
                for (i = columb;i<=size-h-1;i++)
                {
                    array[rows, i] = r++;
                }
                columb = i-1;
                int j;
                for (j = rows+1; j <= size - h-1; j++)
                {
                    array[j, columb] = r++;
                }
                rows = j-1;
                
                int x;
                for (x = columb-1; x >= h+1; x--)
                {
                    array[rows, x] = r++;
                }
                columb = x;
                int y;
                for (y = rows; y > h; y--)
                {
                    array[y, columb] = r++;
                }
                rows = y + 1;
                columb++;
            }
            return array;
        }
    }
}
